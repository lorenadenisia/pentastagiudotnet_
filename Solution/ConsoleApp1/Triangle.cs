﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public class Triangle
    {
        public int FirstSide { get; private set; }
        public int SecondSide { get; private set; }
        public int ThirdSide { get; private set; }

        public void Sides(int firstSide, int secondSide, int thirdSide)
        {
            if (firstSide > 0 && secondSide > 0 && thirdSide > 0)
            {
                FirstSide = firstSide;
                SecondSide = secondSide;
                ThirdSide = thirdSide;

            }
            else
            {
                throw new Exception("The length of a triangle`s side must be a positive number!");
            }
        }
        public bool Equilateral()
        {
            if (FirstSide == SecondSide && SecondSide == ThirdSide)
            {
                return true;
            }
            else
                return false;
        }

        public bool Isosceles()
        {
            if (FirstSide == SecondSide || SecondSide == ThirdSide || ThirdSide == FirstSide)
            {
                return true;
            }
            else
                return false;
        }

        public bool Scalene()
        {
            if (FirstSide != SecondSide && SecondSide != ThirdSide && ThirdSide != FirstSide)
            {
                return true;
            }

            else
                return false;


        }
    }
}
