﻿using Xunit;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System;
using ConsoleApp1;

namespace App.Tests
{
    public class FizzBuzzTests
    {
        [Fact]
        public void FizzBuzz_GetOutput_Method_Returns_Fizz_When_Number_Is_Divisible_By_Only_3()
        {
            var sut = new FizzBuzz();
            sut.GetOutput(3, 6, 9);
            Assert.True(true);
        }

        [Fact]
        public void FizzBuzz_GetOutput_Method_Returns_Buzz_When_Number_Is_Divisible_By_Only_5()
        {
            var sut = new FizzBuzz();
            sut.GetOutput(5, 10, 25);
            Assert.True(sut.GetOutput);

        }

        [Fact]
        public void FizzBuzz_GetOutput_Method_Returns_FizzBuzz_When_Number_Is_Divisible_By_3_And_5()
        {
            var sut = new FizzBuzz();
            sut.GetOutput(15, 30, 45);
            Assert.True(sut.GetOutput);

        }

    }
}
