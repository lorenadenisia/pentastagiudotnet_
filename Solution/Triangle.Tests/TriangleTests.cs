using System;
using Xunit;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleApp1;

namespace TriangleType.Tests
{
    public class TriangleTests
    { 
        [Fact]
        public void Should_Exist()
        {
            var sut = new Triangle();
            Assert.NotNull(sut);
        }

        [Fact]
        public void Should_Have_The_Length_Of_Sides_Greater_Than_Zero()
        {
            var sut = new Triangle();
            sut.Sides(3, 4, 5);
            Assert.True(sut.FirstSide > 0);
            Assert.True(sut.SecondSide > 0);
            Assert.True(sut.ThirdSide > 0);

        }
        [Fact]
        public void Should_Be_Equilateral()
        {
            var sut = new Triangle();
            sut.Sides(10, 10, 10);
            var result = sut.Equilateral();
            Assert.True(result);
        }

        [Fact]
        public void Should_Be_Isosceles()
        {
            var sut = new Triangle();
            sut.Sides(10, 10, 25);
            var result = sut.Isosceles();
            Assert.True(result);
        }

        [Fact]
        public void Should_Be_Scalene()
        {
            var sut = new Triangle();
            sut.Sides(10, 15, 25);
            var result = sut.Scalene();
            Assert.True(result);
        }
    
    }
}
